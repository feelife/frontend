import React from 'react';
import { ActivityOffer } from './ActivityOffer';
import renderer from 'react-test-renderer';
jest.mock('react-redux', () => ({
  useSelector: jest.fn((fn) => fn()),
  useDispatch: jest.fn((fn) => fn()),
}));

describe('SignInForm', () => {
  it('Renders correctly', () => {
    const tree = renderer.create(<ActivityOffer />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
