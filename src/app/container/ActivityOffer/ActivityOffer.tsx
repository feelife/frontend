import React, { useEffect } from 'react';
import { ContentField } from '../../component/uikit/ContentField/ContentField';
import {
  ActivityButtonSection,
  ActivityOfferContainer,
  ActivityText,
} from './ActivityOffer.styles';
import { useDispatch } from 'react-redux';
import {
  completeActivity,
  getActivity,
  takeActivity,
} from '../../store/activity/activity.service';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import { selectToken } from '../../store/auth/auth.selectors';
import {
  selectActivity,
  selectError,
  selectIsLoading,
} from '../../store/activity/activity.selectors';
import { Button } from '../../component/uikit/Button/Button';
import localizations from '../../../settings/localizations';
import { loadActivity } from '../../store/activity/activity.actionCreators';

enum ActivityStatus {
  NO_ACTIVITY,
  NOT_TAKEN_NOT_COMPLETE,
  TAKEN,
  COMPLETED,
}

export const ActivityOffer: React.FC = () => {
  const dispatch = useDispatch();
  const token = useTypedSelector(selectToken);
  const activity = useTypedSelector(selectActivity);
  const error = useTypedSelector(selectError);

  useEffect(() => {
    dispatch(loadActivity());
    dispatch(getActivity(token));
  }, [dispatch, token]);

  const activityStatus = activity
    ? activity.is_completed
      ? ActivityStatus.COMPLETED
      : activity.is_taken
      ? ActivityStatus.TAKEN
      : ActivityStatus.NOT_TAKEN_NOT_COMPLETE
    : ActivityStatus.NO_ACTIVITY;

  const handleNewActivity = () => {
    dispatch(loadActivity());
    dispatch(getActivity(token));
  };

  const handleTakeActivity = () => {
    dispatch(loadActivity());
    dispatch(takeActivity(activity.activity_id, token));
  };

  const handleCompleteActivity = () => {
    dispatch(loadActivity());
    dispatch(completeActivity(activity.activity_id, token));
  };

  const AdditionalInfo = activity && !error && (
    <p>
      {activityStatus === ActivityStatus.COMPLETED
        ? localizations.activity.doneText
        : localizations.activity.estimatedTime(activity?.estimated_time)}
    </p>
  );

  const isLoading = useTypedSelector(selectIsLoading);

  return (
    <ActivityOfferContainer>
      <ContentField
        AdditionalInfo={AdditionalInfo}
        title={localizations.activity.title}
      >
        <ActivityText>{error || activity?.description}</ActivityText>
      </ContentField>
      <ActivityButtonSection>
        {(activityStatus === ActivityStatus.COMPLETED ||
          activityStatus === ActivityStatus.NOT_TAKEN_NOT_COMPLETE ||
          activityStatus === ActivityStatus.NO_ACTIVITY) && (
          <Button onClick={handleNewActivity} width={200} isLoading={isLoading}>
            {localizations.activity.newChallengeText}
          </Button>
        )}
        {activityStatus === ActivityStatus.NOT_TAKEN_NOT_COMPLETE && (
          <Button width={77} onClick={handleTakeActivity} isLoading={isLoading}>
            {localizations.activity.acceptText}
          </Button>
        )}
        {activityStatus === ActivityStatus.TAKEN && (
          <Button
            width={273}
            style={{ background: '#3DCA65' }}
            onClick={handleCompleteActivity}
            isLoading={isLoading}
          >
            {localizations.activity.completeText}
          </Button>
        )}
      </ActivityButtonSection>
    </ActivityOfferContainer>
  );
};
