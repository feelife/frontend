import styled from 'styled-components';
import { StyledButton } from '../../component/uikit/Button/Button.styles';

export const ActivityOfferContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  ${StyledButton} {
    margin-top: 20px;
    :first-of-type {
      margin-right: 20px;
    }
  }
`;

export const ActivityButtonSection = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ActivityText = styled.p`
  font-size: 36px;
  line-height: 42px;
`;
