import { PrivateRoute } from '../component/PrivateRoute';
import config from '../../settings/config';
import { SignIn } from './SignIn/SignIn';
import { SignUp } from './SignUp/SignUp';
import React, { useEffect } from 'react';
import { Switch } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { useTypedSelector } from '../hooks/useTypedSelector';
import { selectIsAuth } from '../store/auth/auth.selectors';
import { ActivityOffer } from './ActivityOffer/ActivityOffer';
import { checkToken } from '../store/auth/auth.service';
import { Achievements } from './Achievements/Achievements';

export const Main: React.FC = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(checkToken(localStorage.getItem('token')));
  }, [dispatch]);

  const isAuth = useTypedSelector(selectIsAuth);

  return (
    <main>
      <Switch>
        <PrivateRoute
          isAuthenticated={isAuth}
          redirectTo={config.app_routes.login}
          exact
          path={config.app_routes.main}
        >
          <ActivityOffer />
        </PrivateRoute>
        <PrivateRoute
          isAuthenticated={isAuth}
          redirectTo={config.app_routes.login}
          exact
          path={config.app_routes.achievements}
        >
          <Achievements />
        </PrivateRoute>
        <PrivateRoute
          isAuthenticated={!isAuth}
          redirectTo={config.app_routes.main}
          exact
          path={config.app_routes.login}
        >
          <SignIn />
        </PrivateRoute>
        <PrivateRoute
          isAuthenticated={!isAuth}
          redirectTo={config.app_routes.main}
          exact
          path={config.app_routes.register}
        >
          <SignUp />
        </PrivateRoute>
      </Switch>
    </main>
  );
};
