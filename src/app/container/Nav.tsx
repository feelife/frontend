import React, { useMemo } from 'react';
import { Navbar } from '../component/navbar/Navbar';
import { useTypedSelector } from '../hooks/useTypedSelector';
import { selectIsAuth, selectToken } from '../store/auth/auth.selectors';
import { Button } from '../component/uikit/Button/Button';
import localizations from '../../settings/localizations';
import { useDispatch } from 'react-redux';
import { logout } from '../store/auth/auth.service';
import { authStart } from '../store/auth/auth.actionCreators';
import { useHistory } from 'react-router-dom';
import config from '../../settings/config';

export const Nav: React.FC = () => {
  const isAuth = useTypedSelector(selectIsAuth);
  const token = useTypedSelector(selectToken);
  const dispatch = useDispatch();
  const history = useHistory();

  const handleLogout = () => {
    dispatch(authStart());
    dispatch(logout(token));
  };

  const handleRedirect = (route: string) => {
    history.push(route);
  };

  const navButtonStyle = useMemo(
    () => ({ background: '#000000', color: '#ffffff' }),
    []
  );

  const navbarElements =
    (isAuth && [
      <Button
        onClick={() => handleRedirect(config.app_routes.achievements)}
        style={navButtonStyle}
      >
        {localizations.achievements.title}
      </Button>,
      <Button
        onClick={() => handleRedirect(config.app_routes.main)}
        style={navButtonStyle}
      >
        {localizations.activity.title}
      </Button>,
      <Button onClick={handleLogout} style={navButtonStyle}>
        {localizations.auth.logoutText}
      </Button>,
    ]) ||
    null;

  return <Navbar elements={navbarElements} />;
};
