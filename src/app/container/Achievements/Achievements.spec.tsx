import React from 'react';
import { Achievements } from './Achievements';
import renderer from 'react-test-renderer';
jest.mock('react-redux', () => ({
  useSelector: jest.fn((fn) => fn()),
  useDispatch: jest.fn((fn) => fn()),
}));

describe('Achievements', () => {
  it('Renders correctly', () => {
    const tree = renderer.create(<Achievements />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
