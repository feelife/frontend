import styled from 'styled-components';
import { StyledButton } from '../../component/uikit/Button/Button.styles';

export const AchievementText = styled.p`
  font-size: 36px;
  line-height: 42px;
`;

export const AchievementsContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  ${StyledButton} {
    margin-top: 20px;
    :first-of-type {
      margin-right: 20px;
    }
  }
`;
