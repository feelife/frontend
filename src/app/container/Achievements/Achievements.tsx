import React, { useEffect, useState } from 'react';
import { ContentField } from '../../component/uikit/ContentField/ContentField';
import localizations from '../../../settings/localizations';
import { useDispatch } from 'react-redux';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import { selectAchievements } from '../../store/achievement/achievement.selectors';
import { getAchievements } from '../../store/achievement/achievement.service';
import { selectToken } from '../../store/auth/auth.selectors';
import { AchievementsContainer, AchievementText } from './Achievements.styles';
import { Button } from '../../component/uikit/Button/Button';

export const Achievements: React.FC = () => {
  const dispatch = useDispatch();
  const token = useTypedSelector(selectToken);

  useEffect(() => {
    dispatch(getAchievements(token));
  }, [dispatch, token]);

  const achievements = useTypedSelector(selectAchievements);
  const [achievementId, setAchievementId] = useState(0);
  const changeAchievementToShow = () => {
    setAchievementId((achievementId + 1) % achievements.length);
  };

  return (
    <AchievementsContainer>
      <ContentField title={localizations.achievements.title}>
        <AchievementText>
          {achievements?.[achievementId]?.name ||
            localizations.achievements.noAchievements}
        </AchievementText>
      </ContentField>
      {achievements?.length > 1 && (
        <Button onClick={changeAchievementToShow}>
          {localizations.achievements.next}
        </Button>
      )}
    </AchievementsContainer>
  );
};
