import React, { useMemo } from 'react';
import {
  SignInData,
  SignInForm,
} from '../../component/authForms/SignInForm/SignInForm';
import { signIn } from '../../store/auth/auth.service';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import config from '../../../settings/config';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import { selectIsLoading } from '../../store/auth/auth.selectors';
import { authStart } from '../../store/auth/auth.actionCreators';
import localizations from '../../../settings/localizations';

export const SignIn: React.FC = () => {
  const dispatch = useDispatch();
  const isLoading = useTypedSelector(selectIsLoading);
  const handleSubmit = (data: SignInData) => {
    dispatch(authStart());
    dispatch(signIn(data));
  };

  const AdditionalInfo = useMemo(
    () => (
      <p>
        {localizations.auth.signIn.additionalInfoTextStart}
        <Link to={config.app_routes.register} style={{ color: '#ffffff' }}>
          {localizations.auth.signIn.additionalInfoTextEnd}
        </Link>
      </p>
    ),
    []
  );

  return (
    <SignInForm
      onSubmit={handleSubmit}
      AdditionalInfo={AdditionalInfo}
      isLoading={isLoading}
    />
  );
};
