import React from 'react';
import { SignIn } from './SignIn';
import renderer from 'react-test-renderer';
import { StaticRouter } from 'react-router';

jest.mock('react-redux', () => ({
  useSelector: jest.fn((fn) => fn()),
  useDispatch: jest.fn((fn) => fn()),
}));

describe('SignIn', () => {
  it('Renders correctly', () => {
    const tree = renderer
      .create(
        <StaticRouter>
          <SignIn />
        </StaticRouter>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
