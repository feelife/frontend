import React from 'react';
import { SignUp } from './SignUp';
import renderer from 'react-test-renderer';
import { StaticRouter } from 'react-router';

jest.mock('react-redux', () => ({
  useSelector: jest.fn((fn) => fn()),
  useDispatch: jest.fn((fn) => fn()),
}));

describe('SignUp', () => {
  it('Renders correctly', () => {
    const tree = renderer
      .create(
        <StaticRouter>
          <SignUp />
        </StaticRouter>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
