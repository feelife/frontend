import React, { useMemo } from 'react';
import {
  SignUpData,
  SignUpForm,
} from '../../component/authForms/SignUpForm/SignUpForm';
import { useDispatch } from 'react-redux';
import { signUp } from '../../store/auth/auth.service';
import config from '../../../settings/config';
import { Link } from 'react-router-dom';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import { selectIsLoading } from '../../store/auth/auth.selectors';
import { authStart } from '../../store/auth/auth.actionCreators';
import localizations from '../../../settings/localizations';

export const SignUp: React.FC = () => {
  const dispatch = useDispatch();
  const isLoading = useTypedSelector(selectIsLoading);
  const handleSubmit = (data: SignUpData) => {
    dispatch(authStart());
    dispatch(signUp(data));
  };

  const AdditionalInfo = useMemo(
    () => (
      <p>
        {localizations.auth.signUp.additionalInfoTextStart}
        <Link to={config.app_routes.login} style={{ color: '#ffffff' }}>
          {localizations.auth.signUp.additionalInfoTextEnd}
        </Link>
      </p>
    ),
    []
  );

  return (
    <SignUpForm
      onSubmit={handleSubmit}
      AdditionalInfo={AdditionalInfo}
      isLoading={isLoading}
    />
  );
};
