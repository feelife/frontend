import { RootState } from '../index';

export const selectAchievements = (state: RootState) =>
  state.achievement.achievements;
