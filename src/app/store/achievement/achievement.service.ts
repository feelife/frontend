import axios from 'axios';
import config from '../../../settings/config';
import {
  errorAchievements,
  setAchievements,
} from './achievement.actionCreators';

export const getAchievements = (token: string) => {
  return (dispatch) => {
    axios
      .get(config.api + config.api_endpoints.get_achievements, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => dispatch(setAchievements(res.data)))
      .catch(() => dispatch(errorAchievements('failed to load')));
  };
};
