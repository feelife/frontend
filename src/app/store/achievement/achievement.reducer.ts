import {
  Achievement,
  AchievementAction,
  AchievementActionTypes,
} from './achievement.actions';

interface AchievementState {
  error: string | null;
  loading: boolean;
  achievements: Achievement[];
}

const initialState: AchievementState = {
  error: null,
  loading: false,
  achievements: [],
};

export const achievementReducer = (
  state = initialState,
  action: AchievementAction
) => {
  switch (action.type) {
    case AchievementActionTypes.LOAD_ACHIEVEMENTS:
      return { ...state, loading: true };
    case AchievementActionTypes.SET_ACHIEVEMENTS:
      return {
        ...state,
        loading: false,
        achievements: action.payload.achievements,
      };
    case AchievementActionTypes.ERROR_ACHIEVEMENTS:
      alert(action.payload.error);
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      };
    default:
      return state;
  }
};
