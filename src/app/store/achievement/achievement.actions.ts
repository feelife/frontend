export interface Achievement {
  name: string;
}

export enum AchievementActionTypes {
  LOAD_ACHIEVEMENTS = 'LOAD_ACHIEVEMENTS',
  SET_ACHIEVEMENTS = 'SET_ACHIEVEMENTS',
  ERROR_ACHIEVEMENTS = 'ERROR_ACHIEVEMENTS',
}

export interface LoadAchievementsAction {
  type: AchievementActionTypes.LOAD_ACHIEVEMENTS;
}

export interface SetAchievementsAction {
  type: AchievementActionTypes.SET_ACHIEVEMENTS;
  payload: { achievements: Achievement[] };
}

export interface ErrorAchievementsAction {
  type: AchievementActionTypes.ERROR_ACHIEVEMENTS;
  payload: { error: string };
}

export type AchievementAction =
  | LoadAchievementsAction
  | SetAchievementsAction
  | ErrorAchievementsAction;
