import {
  Achievement,
  AchievementActionTypes,
  ErrorAchievementsAction,
  LoadAchievementsAction,
  SetAchievementsAction,
} from './achievement.actions';

export const loadAchievements = () =>
  ({
    type: AchievementActionTypes.LOAD_ACHIEVEMENTS,
  } as LoadAchievementsAction);

export const setAchievements = (achievements: Achievement[]) =>
  ({
    type: AchievementActionTypes.SET_ACHIEVEMENTS,
    payload: { achievements },
  } as SetAchievementsAction);

export const errorAchievements = (error: string) =>
  ({
    type: AchievementActionTypes.ERROR_ACHIEVEMENTS,
    payload: { error },
  } as ErrorAchievementsAction);
