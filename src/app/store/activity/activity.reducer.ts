import {
  Activity,
  ActivityAction,
  ActivityActionsTypes,
} from './activity.actions';

interface ActivityState {
  loading: boolean;
  error: string | null;
  activity: Activity;
}

const initialState: ActivityState = {
  loading: false,
  error: null,
  activity: null,
};

export const activityReducer = (
  state = initialState,
  action: ActivityAction
) => {
  switch (action.type) {
    case ActivityActionsTypes.LOAD_ACTIVITY:
      return { ...state, loading: true };
    case ActivityActionsTypes.SET_ACTIVITY:
      return {
        ...state,
        loading: false,
        activity: action.payload.activity,
        error: null,
      };
    case ActivityActionsTypes.ERROR_ACTIVITY:
      alert(action.payload.error);
      return { ...state, loading: false, error: action.payload.error };
    default:
      return state;
  }
};
