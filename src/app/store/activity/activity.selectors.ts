import { RootState } from '../index';

export const selectActivity = (state: RootState) => state.activity.activity;
export const selectIsLoading = (state: RootState) => state.activity.loading;
export const selectError = (state: RootState) => state.activity.error;
