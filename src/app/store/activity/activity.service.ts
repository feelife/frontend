import axios from 'axios';
import config from '../../../settings/config';
import { errorActivity, setActivity } from './activity.actionCreators';

export const getActivity = (token: string) => {
  return (dispatch) => {
    axios
      .get(config.api + config.api_endpoints.load_activity, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => dispatch(setActivity(res.data)))
      .catch((err) =>
        dispatch(
          errorActivity(err?.response?.data?.message || 'failed to load')
        )
      );
  };
};

export const takeActivity = (activity_id: number, token: string) => {
  return (dispatch) => {
    axios
      .post(
        config.api + config.api_endpoints.take_activity,
        { activity_id },
        { headers: { Authorization: `Bearer ${token}` } }
      )
      .then((res) => dispatch(setActivity(res.data)))
      .catch((err) =>
        dispatch(
          errorActivity(err?.response?.data?.message || 'failed to take')
        )
      );
  };
};

export const completeActivity = (activity_id: number, token: string) => {
  return (dispatch) => {
    axios
      .post(
        config.api + config.api_endpoints.complete_activity,
        { activity_id },
        { headers: { Authorization: `Bearer ${token}` } }
      )
      .then((res) => dispatch(setActivity(res.data)))
      .catch((err) =>
        dispatch(
          errorActivity(err?.response?.data?.message || 'failed to complete')
        )
      );
  };
};
