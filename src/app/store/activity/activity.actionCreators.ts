import {
  Activity,
  ActivityActionsTypes,
  ErrorActivityAction,
  LoadActivityAction,
  SetActivityAction,
} from './activity.actions';

export const loadActivity = () =>
  ({ type: ActivityActionsTypes.LOAD_ACTIVITY } as LoadActivityAction);

export const setActivity = (activity: Activity) =>
  ({
    type: ActivityActionsTypes.SET_ACTIVITY,
    payload: { activity },
  } as SetActivityAction);

export const errorActivity = (error: string) =>
  ({
    type: ActivityActionsTypes.ERROR_ACTIVITY,
    payload: { error },
  } as ErrorActivityAction);
