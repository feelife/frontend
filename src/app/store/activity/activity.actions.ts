export interface Activity {
  description: string;
  estimated_time: number;
  is_completed: boolean;
  is_taken: boolean;
  name: string;
  activity_id: number;
}

export enum ActivityActionsTypes {
  LOAD_ACTIVITY = 'LOAD_ACTIVITY',
  SET_ACTIVITY = 'SET_ACTIVITY',
  ERROR_ACTIVITY = 'ERROR_ACTIVITY',
}

export interface LoadActivityAction {
  type: ActivityActionsTypes.LOAD_ACTIVITY;
}

export interface SetActivityAction {
  type: ActivityActionsTypes.SET_ACTIVITY;
  payload: { activity: Activity };
}

export interface ErrorActivityAction {
  type: ActivityActionsTypes.ERROR_ACTIVITY;
  payload: { error: string };
}

export type ActivityAction =
  | LoadActivityAction
  | SetActivityAction
  | ErrorActivityAction;
