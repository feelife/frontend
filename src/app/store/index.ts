import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { authReducer } from './auth/auth.reducer';
import { activityReducer } from './activity/activity.reducer';
import { achievementReducer } from './achievement/achievement.reducer';

const rootReducer = combineReducers({
  auth: authReducer,
  activity: activityReducer,
  achievement: achievementReducer,
});

export const store = createStore(rootReducer, applyMiddleware(thunk));

export type RootState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof store.dispatch;
