import axios from 'axios';
import config from '../../../settings/config';
import { SignInData } from '../../component/authForms/SignInForm/SignInForm';
import { authError, authLogout, authSuccess } from './auth.actionCreators';
import { SignUpData } from '../../component/authForms/SignUpForm/SignUpForm';

export const signIn = (data: SignInData) => {
  return (dispatch) => {
    axios
      .post(config.api + config.api_endpoints.login, data)
      .then((res) => dispatch(authSuccess(res.data.token)))
      .catch((err) =>
        dispatch(authError(err?.response?.data?.message || 'failed to connect'))
      );
  };
};

export const signUp = (data: SignUpData) => {
  return (dispatch) => {
    axios
      .post(config.api + config.api_endpoints.register, data)
      .then((res) => dispatch(authSuccess(res.data.token)))
      .catch((err) =>
        dispatch(authError(err?.response?.data?.message || 'failed to connect'))
      );
  };
};

export const logout = (token: string) => {
  return (dispatch) => {
    axios
      .get(config.api + config.api_endpoints.logout, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(() => dispatch(authLogout()))
      .catch((err) =>
        dispatch(authError(err?.response?.data?.message || 'failed to connect'))
      );
  };
};

export const checkToken = (token: string) => {
  return (dispatch) => {
    axios
      .get(config.api + config.api_endpoints.check_token, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) =>
        res.data ? dispatch(authSuccess(token)) : dispatch(authLogout())
      );
  };
};
