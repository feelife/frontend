import { AuthAction, AuthActionsTypes } from './auth.actions';

interface AuthState {
  loading: boolean;
  error: null | string;
  token: string;
}

const initialState: AuthState = { error: null, loading: false, token: '' };

export const authReducer = (state = initialState, action: AuthAction) => {
  switch (action.type) {
    case AuthActionsTypes.AUTH_START:
      return { ...state, loading: true };
    case AuthActionsTypes.AUTH_SUCCESS:
      localStorage.setItem('token', action.payload.token);
      return {
        ...state,
        loading: false,
        token: action.payload.token,
        error: null,
      };
    case AuthActionsTypes.AUTH_ERROR:
      alert(action.payload.error);
      return { ...state, loading: false, error: action.payload.error };
    case AuthActionsTypes.LOGOUT:
      localStorage.removeItem('token');
      return { ...state, loading: false, token: '', error: null };
    default:
      return state;
  }
};
