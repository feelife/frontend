import {
  AuthActionsTypes,
  AuthErrorAction,
  AuthStartAction,
  AuthSuccessAction,
  LogoutAction,
} from './auth.actions';

export const authStart = () =>
  ({ type: AuthActionsTypes.AUTH_START } as AuthStartAction);

export const authSuccess = (token: string) =>
  ({
    type: AuthActionsTypes.AUTH_SUCCESS,
    payload: { token },
  } as AuthSuccessAction);

export const authError = (error: string) =>
  ({
    type: AuthActionsTypes.AUTH_ERROR,
    payload: { error },
  } as AuthErrorAction);

export const authLogout = () =>
  ({
    type: AuthActionsTypes.LOGOUT,
  } as LogoutAction);
