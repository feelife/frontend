import { RootState } from '../index';

export const selectIsLoading = (state: RootState) => state.auth.loading;

export const selectToken = (state: RootState) => state.auth.token;

export const selectIsAuth = (state: RootState) => !!state.auth.token;
