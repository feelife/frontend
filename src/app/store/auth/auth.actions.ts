export enum AuthActionsTypes {
  AUTH_START = 'AUTH_START',
  AUTH_SUCCESS = 'AUTH_SUCCESS',
  AUTH_ERROR = 'AUTH_ERROR',
  LOGOUT = 'LOGOUT',
}

export interface AuthStartAction {
  type: AuthActionsTypes.AUTH_START;
}

export interface AuthSuccessAction {
  type: AuthActionsTypes.AUTH_SUCCESS;
  payload: { token: string };
}

export interface AuthErrorAction {
  type: AuthActionsTypes.AUTH_ERROR;
  payload: { error: string };
}

export interface LogoutAction {
  type: AuthActionsTypes.LOGOUT;
}

export type AuthAction =
  | AuthStartAction
  | AuthSuccessAction
  | AuthErrorAction
  | LogoutAction;
