import React from 'react';
import { useForm } from 'react-hook-form';
import { StyledAuthForm } from '../AuthForm.styles';
import { ContentField } from '../../uikit/ContentField/ContentField';
import { Input } from '../../uikit/Input/Input';
import { Button } from '../../uikit/Button/Button';
import localizations from '../../../../settings/localizations';

export interface SignUpData {
  username: string;
  email: string;
  password: string;
}

interface FormError {
  errorType: string;
  message: string;
}

export interface SignUpFormProps {
  /** Field additional info */
  AdditionalInfo?: JSX.Element;
  /** Actions on submit */
  onSubmit: (data: SignUpData) => unknown;
  /** Errors in form */
  errors?: FormError[];
  /** Is waiting for server response */
  isLoading?: boolean;
}

export const SignUpForm: React.FC<SignUpFormProps> = ({
  AdditionalInfo,
  onSubmit,
  errors,
  isLoading,
}) => {
  const { handleSubmit, register } = useForm();

  const handleFormSubmit = (data: SignUpData) => onSubmit(data);

  return (
    <StyledAuthForm onSubmit={handleSubmit(handleFormSubmit)}>
      <ContentField title="Sign Up" AdditionalInfo={AdditionalInfo}>
        <Input
          type="text"
          required
          {...register('username')}
          placeholder="username"
        />
        <Input
          type="email"
          required
          {...register('email')}
          placeholder="email"
        />
        <Input
          type="password"
          required
          minLength={1}
          {...register('password')}
          placeholder="password"
        />
      </ContentField>
      <Button type="submit" isLoading={isLoading}>
        {localizations.auth.buttonText}
      </Button>
    </StyledAuthForm>
  );
};
