import React from 'react';
import { SignUpForm } from './SignUpForm';
import renderer from 'react-test-renderer';

describe('SignUpForm', () => {
  it('Renders correctly', () => {
    const tree = renderer
      .create(<SignUpForm onSubmit={() => alert(1)} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
