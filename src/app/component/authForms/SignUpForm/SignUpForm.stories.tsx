import React from 'react';
import { SignUpForm } from './SignUpForm';

export default {
  title: 'Sign Up Form',
  component: SignUpForm,
};

export const Default = () => (
  <div
    style={{
      background: 'linear-gradient(192.45deg, #F17CED -13.1%, #000000 92.82%)',
      padding: '30px',
    }}
  >
    <SignUpForm
      AdditionalInfo={<p style={{ margin: 0 }}>Additional Info</p>}
      onSubmit={({ username, email, password }) =>
        alert(`${username}:${email}:${password}`)
      }
    />
  </div>
);
