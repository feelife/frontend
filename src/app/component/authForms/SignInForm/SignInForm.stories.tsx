import React from 'react';
import { SignInForm } from './SignInForm';

export default {
  title: 'Sign In Form',
  component: SignInForm,
};

export const Default = () => (
  <div
    style={{
      background: 'linear-gradient(192.45deg, #F17CED -13.1%, #000000 92.82%)',
      padding: '30px',
    }}
  >
    <SignInForm
      AdditionalInfo={<p style={{ margin: 0 }}>Additional Info</p>}
      onSubmit={({ email, password }) => alert(`${email}:${password}`)}
    />
  </div>
);
