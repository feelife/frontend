import React from 'react';
import { ContentField } from '../../uikit/ContentField/ContentField';
import { Input } from '../../uikit/Input/Input';
import { Button } from '../../uikit/Button/Button';
import localizations from '../../../../settings/localizations';
import { StyledAuthForm } from '../AuthForm.styles';
import { useForm } from 'react-hook-form';

export interface SignInData {
  email: string;
  password: string;
}

export interface SignInFormProps {
  /** Field additional info */
  AdditionalInfo?: JSX.Element;
  /** Actions on submit */
  onSubmit: (data: SignInData) => unknown;
  /** Is waiting for server response */
  isLoading?: boolean;
}

export const SignInForm: React.FC<SignInFormProps> = ({
  AdditionalInfo,
  onSubmit,
  isLoading,
}) => {
  const { handleSubmit, register } = useForm();

  const handleFormSubmit = (data: SignInData) => onSubmit(data);

  return (
    <StyledAuthForm onSubmit={handleSubmit(handleFormSubmit)}>
      <ContentField title="Sign In" AdditionalInfo={AdditionalInfo}>
        <Input
          type="email"
          required
          {...register('email')}
          placeholder="email"
        />
        <Input
          type="password"
          required
          minLength={1}
          {...register('password')}
          placeholder="password"
        />
      </ContentField>
      <Button type="submit" isLoading={isLoading}>
        {localizations.auth.buttonText}
      </Button>
    </StyledAuthForm>
  );
};
