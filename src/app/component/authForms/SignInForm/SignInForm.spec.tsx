import React from 'react';
import { SignInForm } from './SignInForm';
import renderer from 'react-test-renderer';

describe('SignInForm', () => {
  it('Renders correctly', () => {
    const tree = renderer
      .create(<SignInForm onSubmit={() => alert(1)} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
