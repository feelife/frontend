import styled from 'styled-components';
import { StyledButton } from '../uikit/Button/Button.styles';

export const StyledAuthForm = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;

  ${StyledButton} {
    margin-top: 20px;
  }
`;
