import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export interface PrivateRouteProps {
  isAuthenticated: boolean;
  redirectTo: string;
  [rest: string]: any;
}

export const PrivateRoute: React.FC<PrivateRouteProps> = ({
  children,
  isAuthenticated,
  redirectTo,
  ...rest
}) => (
  <Route
    {...rest}
    render={({ location }) =>
      isAuthenticated ? (
        children
      ) : (
        <Redirect
          to={{
            pathname: `${redirectTo}`,
            state: { from: location },
          }}
        />
      )
    }
  />
);
