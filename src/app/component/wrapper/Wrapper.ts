import styled from 'styled-components';

export const Wrapper = styled.div`
  background: linear-gradient(192.45deg, #f17ced -13.1%, #000000 92.82%);
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
