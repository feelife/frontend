import React from 'react';
import {
  ContentFieldContainer,
  ContentFieldTitle,
  StyledContentField,
} from './ContentField.styles';

export interface ContentFieldProps {
  /** Title of the field (displayed at the top) */
  title?: string;
  /** Field additional info (displayed ad the bottom) */
  AdditionalInfo?: JSX.Element;
}

export const ContentField: React.FC<ContentFieldProps> = ({
  title,
  children,
  AdditionalInfo,
}) => (
  <ContentFieldContainer>
    {title && <ContentFieldTitle>{title}</ContentFieldTitle>}
    <StyledContentField>{children}</StyledContentField>
    {AdditionalInfo}
  </ContentFieldContainer>
);
