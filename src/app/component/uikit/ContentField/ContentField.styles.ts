import styled from 'styled-components';

export const ContentFieldContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  color: #ffffff;
`;

export const ContentFieldTitle = styled.h2`
  font-size: 36px;
  margin: 0 0 20px;
`;

export const StyledContentField = styled.div`
  width: 235px;
  padding: 25px 30px 70px 30px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  background-color: #ffffff;
  color: #000000;
  border-radius: 30px;
  margin: 0 0 10px;
  > * {
    margin-top: 45px;
  }
  @media (min-width: 900px) {
    width: 400px;
  }
`;
