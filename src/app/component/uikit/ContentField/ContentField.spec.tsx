import React from 'react';
import { ContentField } from './ContentField';
import renderer from 'react-test-renderer';

describe('ContentField', () => {
  it('renders correctly', () => {
    const tree = renderer.create(<ContentField />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
