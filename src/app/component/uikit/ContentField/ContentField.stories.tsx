import React from 'react';
import { ContentField } from './ContentField';
import { Input } from '../Input/Input';

export default {
  title: 'ContentField',
  component: ContentField,
};

export const Default = () => (
  <div
    style={{
      background: 'linear-gradient(192.45deg, #F17CED -13.1%, #000000 92.82%)',
      padding: '30px',
    }}
  >
    <ContentField />
  </div>
);

export const Custom = () => (
  <div
    style={{
      background: 'linear-gradient(192.45deg, #F17CED -13.1%, #000000 92.82%)',
    }}
  >
    <ContentField
      title="Log in"
      AdditionalInfo={<p style={{ margin: 0 }}>Don`t have account yet?</p>}
    >
      <Input type="text" width="235px" />
      <Input type="password" width="235px" />
    </ContentField>
  </div>
);
