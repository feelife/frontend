import React from 'react';

import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import { Button } from './Button';
import { Spinner, StyledButton } from './Button.styles';

describe('Button', () => {
  it('Renders correctly', () => {
    const tree = renderer
      .create(
        <Button handleClick={() => alert('Click')} width={500}>
          Click
        </Button>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('Click to the button works', () => {
    window.alert = jest.fn();
    const btn = mount(
      <Button handleClick={() => alert('Click')} width={500}>
        Click
      </Button>
    );
    btn.find(StyledButton).simulate('click');
    expect(alert).toBeCalledWith('Click');
  });
  it('Spinner appears', () => {
    const btn = mount(
      <Button handleClick={() => alert('Click')} isLoading={true}>
        Click
      </Button>
    );
    expect(btn.find(Spinner)).toHaveLength(1);
  });
});
