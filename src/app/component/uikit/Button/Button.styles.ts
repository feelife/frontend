import styled, { keyframes } from 'styled-components';

export const StyledButton = styled.button<{ width }>`
  height: 40px;
  padding: 10px 30px;
  border-radius: 30px;
  border: none;
  background-color: #ffffff;
  color: #000000;
  font-size: 20px;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  ${({ width }) => width && `width: ${width}px;`}
  :hover,
  :focus {
    opacity: 0.8;
  }
`;

export const rotate = keyframes`
  from {
    transform: rotate(0turn);
  }

  to {
    transform: rotate(1turn);
  }
`;

export const Spinner = styled.span`
  width: 16px;
  height: 16px;
  border: 4px solid transparent;
  border-top-color: #000000;
  border-radius: 50%;
  animation: ${rotate} 1s ease infinite;
`;
