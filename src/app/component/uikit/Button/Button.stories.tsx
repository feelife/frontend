import React from 'react';
import { Button } from './Button';

export default {
  title: 'Button',
  component: Button,
};

export const Default = () => (
  <div
    style={{
      background: 'linear-gradient(192.45deg, #F17CED -13.1%, #000000 92.82%)',
      padding: '30px',
    }}
  >
    <Button handleClick={() => alert('Click')}>new challenge</Button>
  </div>
);

export const WithWidth = () => (
  <div
    style={{
      background: 'linear-gradient(192.45deg, #F17CED -13.1%, #000000 92.82%)',
      padding: '30px',
    }}
  >
    <Button handleClick={() => alert('Click')} width={500}>
      Click
    </Button>
  </div>
);

export const WithSpinner = () => (
  <div
    style={{
      background: 'linear-gradient(192.45deg, #F17CED -13.1%, #000000 92.82%)',
      padding: '30px',
    }}
  >
    <Button handleClick={() => alert('Click')} width={100} isLoading={true}>
      Click
    </Button>
  </div>
);
