import React, { ButtonHTMLAttributes } from 'react';
import { Spinner, StyledButton } from './Button.styles';

export interface ButtonProps extends ButtonHTMLAttributes<unknown> {
  /** Click action */
  handleClick?: (event: MouseEvent) => void;
  /** Button width in px*/
  width?: number;
  /** Is the spinner icon displayed */
  isLoading?: boolean;
}

export const Button: React.FC<ButtonProps> = ({
  handleClick,
  children,
  width,
  isLoading,
  ...other
}) => (
  <StyledButton onClick={handleClick} width={width} {...other}>
    {isLoading ? <Spinner /> : children}
  </StyledButton>
);
