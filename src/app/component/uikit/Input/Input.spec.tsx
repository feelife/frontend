import React from 'react';
import renderer from 'react-test-renderer';
import { Input } from './Input';

describe('Input', () => {
  describe('Renders correctly', () => {
    it('Default input', () => {
      const tree = renderer
        .create(<Input type="text" placeholder="text" />)
        .toJSON();
      expect(tree).toMatchSnapshot();
    });
    it('Input with error', () => {
      const tree = renderer
        .create(<Input type="text" placeholder="text" error="text" />)
        .toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
});
