import React from 'react';
import { Input } from './Input';

export default {
  title: 'Input',
  component: Input,
};

export const Default = () => <Input type="text" />;
export const WithWidth = () => <Input type="text" width="100px" />;
export const WithPlaceholder = () => (
  <Input type="text" placeholder="username" />
);
export const TypePassword = () => <Input type="password" />;
export const WithError = () => <Input type="text" error={'input error'} />;
