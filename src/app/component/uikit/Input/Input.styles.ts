import styled, { css } from 'styled-components';
import { InputProps } from './Input';

export const getErrorStyles = (error: string) => css`
  position: relative;
  &:after {
    content: '${error}';
    position: absolute;
    top: 35px;
    left: 0;
    color: #fb5c5c;
  }
`;

export const InputWrapper = styled.div<{ error }>`
  ${({ error }) => error && getErrorStyles(error)}
`;

export const StyledInput = styled.input<InputProps>`
  width: 100%;
  ${({ width }) => width && `width: ${width};`}
  ${({ error }) => error && `color: #fb5c5c;`}
  border: none;
  outline: none;
  border-bottom: 3px solid #545454;
  background: transparent;
  padding: 0;
  font-size: 24px;
  :hover,
  :focus {
    opacity: 0.88;
  }
`;
