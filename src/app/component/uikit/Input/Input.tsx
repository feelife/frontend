import React, { InputHTMLAttributes } from 'react';
import { InputWrapper, StyledInput } from './Input.styles';

export interface InputProps extends InputHTMLAttributes<unknown> {
  /** Error text */
  error?: string;
  /** Input width */
  width?: string;
}

export const Input = React.forwardRef<HTMLInputElement, InputProps>(
  (props, ref) => (
    <InputWrapper error={props.error}>
      <StyledInput {...props} ref={ref} />
    </InputWrapper>
  )
);
