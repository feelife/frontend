import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import { Navbar } from './Navbar';
import { NavbarCloseIcon, NavbarMenuIcon } from './Navbar.styles';
import { NavbarMenu } from './NavbarMenu';

describe('Navbar', () => {
  it('Renders correctly', () => {
    const tree = renderer
      .create(<Navbar title="title" elements={[<p>Link</p>]} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('Opening/closing menu', () => {
    const nav = mount(<Navbar title="title" elements={[<p>Link</p>]} />);
    expect(nav.find(NavbarMenu)).toHaveLength(0);
    nav.find(NavbarMenuIcon).simulate('click');
    expect(nav.find(NavbarMenu)).toHaveLength(1);
    nav.find(NavbarCloseIcon).simulate('click');
    expect(nav.find(NavbarMenu)).toHaveLength(0);
  });
});
