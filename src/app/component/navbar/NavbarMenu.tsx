import React from 'react';
import ReactDOM from 'react-dom';
import {
  Background,
  NavbarCloseIcon,
  NavbarMenuItem,
  StyledNavbarMenu,
} from './Navbar.styles';

export interface NavbarMenuProps {
  /** Navigation elements */
  elements?: JSX.Element[];
  /** Actions on close */
  onClose: () => void;
}

export const NavbarMenu: React.FC<NavbarMenuProps> = ({
  elements,
  onClose,
}) => {
  return ReactDOM.createPortal(
    <Background onClick={onClose}>
      <StyledNavbarMenu onClick={(e) => e.stopPropagation()}>
        <NavbarCloseIcon onClick={onClose} />
        {elements?.map((element, idx) => (
          <NavbarMenuItem onClick={onClose} key={idx}>
            {element}
          </NavbarMenuItem>
        ))}
      </StyledNavbarMenu>
    </Background>,
    document.querySelector('body')
  );
};
