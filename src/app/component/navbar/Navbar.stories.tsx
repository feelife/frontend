import React from 'react';
import { Navbar } from './Navbar';

export default {
  title: 'Navar',
  component: Navbar,
};

export const Default = () => (
  <Navbar
    title="username"
    elements={[<p>Link 1</p>, <p>Link 2</p>, <p>Link 3</p>]}
  />
);
