import styled from 'styled-components';

export const NavbarContainer = styled.div`
  position: relative;
`;

export const StyledNavbar = styled.nav`
  box-sizing: border-box;
  width: 100%;
  height: 70px;
  background: #7a3077;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 20px;
  color: #ffffff;
  position: fixed;
  top: 0;
  left: 0;
`;

export const NavbarTitle = styled.p`
  font-weight: 700;
  font-size: 36px;
  justify-self: flex-start;
  margin: 0;
`;

export const Line = styled.div<{ rotate; color }>`
  width: 40px;
  height: 5px;
  background: #ffffff;
  transform: rotate(${({ rotate }) => rotate});
  background: ${({ color }) => color};
`;

export const NavbarCloseIcon = styled.div`
  cursor: pointer;
  width: 32px;
  height: 32px;
  position: relative;
  margin-bottom: 40px;
  :hover {
    opacity: 0.8;
  }
  :before,
  :after {
    position: absolute;
    left: 15px;
    content: ' ';
    height: 33px;
    width: 5px;
    background-color: #000000;
  }
  :before {
    transform: rotate(45deg);
  }
  :after {
    transform: rotate(-45deg);
  }
`;

export const NavbarMenuIcon = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  width: 40px;
  height: 30px;
  cursor: pointer;
`;

export const StyledNavbarMenu = styled.div`
  box-sizing: border-box;
  height: 100vh;
  background: #ffffff;
  padding: 20px;
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  width: 320px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  @media (max-width: 600px) {
    width: 100%;
  }
`;

export const NavbarMenuItem = styled.div`
  margin-top: 30px;
`;

export const Background = styled.div`
  position: absolute;
  top: 0;
  width: 100vw;
  height: 100vh;
  background: rgba(0, 0, 0, 0.8);
`;
