import React, { useState } from 'react';
import {
  StyledNavbar,
  NavbarTitle,
  Line,
  NavbarMenuIcon,
  NavbarContainer,
} from './Navbar.styles';
import { NavbarMenu } from './NavbarMenu';

export interface NavbarProps {
  /** Navigation elements */
  elements?: JSX.Element[];
  /** NavBar title */
  title?: string;
}

export const Navbar: React.FC<NavbarProps> = ({ elements, title }) => {
  const [isMenuOpened, setIsMenuOpened] = useState(false);

  const toggleMenu = () => setIsMenuOpened(!isMenuOpened);

  return (
    <NavbarContainer>
      <StyledNavbar>
        <NavbarTitle>{title}</NavbarTitle>
        {elements && (
          <NavbarMenuIcon tabIndex={0} onClick={toggleMenu}>
            <Line />
            <Line />
            <Line />
          </NavbarMenuIcon>
        )}
      </StyledNavbar>
      {isMenuOpened && <NavbarMenu onClose={toggleMenu} elements={elements} />}
    </NavbarContainer>
  );
};
