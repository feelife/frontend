import React from 'react';
import { Provider } from 'react-redux';
import { store } from './store';
import { BrowserRouter as Router } from 'react-router-dom';
import { Wrapper } from './component/wrapper/Wrapper';
import { Main } from './container/Main';
import { Nav } from './container/Nav';

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Wrapper>
          <Nav />
          <Main />
        </Wrapper>
      </Router>
    </Provider>
  );
}

export default App;
