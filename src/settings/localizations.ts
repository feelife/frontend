const localizations = {
  auth: {
    buttonText: 'next',
    signIn: {
      additionalInfoTextStart: "Don't have an account yet? Then ",
      additionalInfoTextEnd: 'Sign Up',
    },
    signUp: {
      additionalInfoTextStart: 'Already have an account? Then ',
      additionalInfoTextEnd: 'Sign In',
    },
    logoutText: 'Logout',
  },
  activity: {
    newChallengeText: 'new challenge',
    acceptText: '👍',
    completeText: '✅',
    doneText: 'Activity is done!',
    estimatedTime: (time: number) => `Estimated time - ${time} min`,
    title: 'Activity',
  },
  achievements: {
    title: 'Achievements',
    noAchievements: 'You don`t have achievements',
    next: 'next',
  },
};

export default localizations;
