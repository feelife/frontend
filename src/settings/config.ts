const config = {
  api: 'https://staging-feelife.cloudns.ph/api',
  api_endpoints: {
    register: '/auth/register',
    login: '/auth/login',
    logout: '/auth/logout',
    load_activity: '/activity/new',
    take_activity: '/activity/take',
    complete_activity: '/activity/complete',
    check_token: '/token/check',
    get_achievements: '/achievement/my',
  },
  app_routes: {
    main: '/',
    login: '/auth/login',
    register: '/auth/register',
    achievements: '/achievements',
  },
};

export default config;
