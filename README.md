# Info

The repository contains frontend part of [feelife](https://gitlab.com/feelife/feelife) project.

## How to run

1) Install [NodeJS](https://nodejs.org/) on your computer
2) Install all project dependencies using command `npm i` inside project directory
3) Run any available script (see the section below)

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm run test`

Launches all tests of the project.\
You will also see test coverage.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

### `npm run storybook`
Launches website where you can look at all UI components.

